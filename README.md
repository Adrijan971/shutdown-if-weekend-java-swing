This small GUI app was done for a client (BMW Service Rezac Novi Sad).
They needed their pc's to start before they open the shop/service so they don't have to do it every day except weekends.
Bios settings were configured to start the machine each day(there was no option to set which days in a week).
So .jar file of this project was put in a startup folder(windows) which would mean that it is going to start after the windows boots up.
When the app is started it check's the date and if it is a saturday or a sunday it starts shutdown but it gives you 15s to cancel the shutdown.
If it is not a weekend it just exits the app.

Date of the installation on pc's : march 2020.   So far , no problems.
