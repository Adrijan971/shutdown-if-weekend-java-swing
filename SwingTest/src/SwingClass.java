import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class SwingClass {
	
	// broj sekundi koji hoce da se sleepuje(x)
	// number of seconds to sleep, just func for sleeping. 
	public static void sleep(int x) {
		try {
			Thread.sleep(x*1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	// Proverava da li je vikend ili ne
	// Checks if it is weekend or not
	public static boolean proveriDan() {
		//true - weekend / vikend
		//false - not / nije
		Calendar calendar = Calendar.getInstance();
		int day = calendar.get(Calendar.DAY_OF_WEEK); 
		if ( day==1 || day==7 ){
			return true;
		}
		else {
			return false;
		}
	}
	
	// gasi racunar
	// shuts down the pc
	public static void ugasiKomp() {
		Runtime runtime = Runtime.getRuntime();
	    try {
			Process proc = runtime.exec("shutdown -s -t 0");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    System.exit(0);
	}
	
	
	public static void gui(JLabel l33) {
		
		JFrame frame = new JFrame("Potvrda");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    frame.setSize(600,500);
	    frame.setResizable(false);
	    frame.setLocationRelativeTo(null);
	    
	    JPanel panel = new JPanel();
	    panel.setLayout(null);
	    frame.add(panel);
	    
	    JButton button = new JButton("STOP");
	    button.setBackground(Color.RED);
	    button.setFont(new Font("Arial", Font.PLAIN, 40));
	    button.setBounds(200, 200, 200, 100);
	    button.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("button activated");
				System.exit(0);
			}
		});
	    panel.add(button);
	    l33.setText("RACUNAR CE SE UGASITI ZA 15s!");
	    //JLabel l1 = new JLabel("RACUNAR CE SE UGASITI ZA 15s!");
	    l33.setFont(new Font("Arial", Font.PLAIN, 17));
	    l33.setBounds(60,50,500,50);
	    panel.add(l33);
	    
	    JLabel l2 = new JLabel("ZA PREKID GASENJA RACUNARA PRITISNUTI STOP DUGME!");
	    l2.setFont(new Font("Arial", Font.PLAIN, 17));
	    l2.setBounds(60,100,500,50);
	    panel.add(l2);
	    
	    frame.setVisible(true);
	}
	
	public static void main(String[] args) {
		
		JLabel l3 = new JLabel();
		// ako je vikend , pokrece timer funkciju za gasenje
		// if it is weekend then start timer to shutdown
		if(proveriDan()==true) {
			
			TimerTask task = new TimerTask() {
	
				@Override
				public void run() {
					sleep(5);
					l3.setText("RACUNAR CE SE UGASITI ZA 10s!");
					sleep(5);
					l3.setText("RACUNAR CE SE UGASITI ZA 5s!");
					sleep(5);
					System.out.println("sleep finished");
					ugasiKomp();
				}};
			
			Timer timer = new Timer();
			
			// opens gui part of the app
			gui(l3);
			
			timer.schedule(task, 0);
		}
		// ako nije vikend samo izlazi iz app a gui se ni ne otvara
		// if it is not a weekend , then just exit the app (gui will not even appear)
		else {
			System.exit(0);
		}
	}

}
